package timeutil

import (
	"testing"
	"time"
)

func TestParseUnixTimestamp(t *testing.T) {
	cases := []struct {
		name, input string
		output      time.Time
		expectErr   bool
	}{
		{
			name:      "postive unix timestamp",
			input:     "1501961870",
			output:    time.Date(2017, 8, 5, 19, 37, 50, 0, time.UTC),
			expectErr: false,
		},
		{
			name:      "zero unix timestamp",
			input:     "0",
			output:    time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC),
			expectErr: false,
		},
		{
			name:      "negative unix timestamp",
			input:     "-1",
			output:    time.Date(1969, 12, 31, 23, 59, 59, 0, time.UTC),
			expectErr: false,
		},
		{
			name:      "non int string",
			input:     "foobar",
			output:    time.Time{},
			expectErr: true,
		},
	}
	for _, tc := range cases {
		actual, err := ParseUnixSeconds(tc.input)
		if tc.expectErr && err == nil {
			t.Errorf("expected an err for %s: input: %s", tc.name, tc.input)
		}
		if !tc.expectErr && err != nil {
			t.Errorf("didn't expect err for %s: input: %s , expected: %s",
				tc.name,
				tc.input,
				tc.output,
			)
		}
		if actual != tc.output {
			t.Errorf("%s: input: %s , actual: %s , expected: %s",
				tc.name,
				tc.input,
				actual.Format(time.RFC3339),
				tc.output.Format(time.RFC3339),
			)
		}
	}
}

func TestSameDate(t *testing.T) {
	cases := []struct {
		name     string
		a, b     time.Time
		expected bool
	}{
		{
			name:     "same date",
			a:        time.Date(2017, 8, 1, 0, 0, 0, 0, time.UTC),
			b:        time.Date(2017, 8, 1, 0, 23, 0, 0, time.UTC),
			expected: true,
		},
		{
			name:     "different date",
			a:        time.Date(2017, 8, 1, 0, 0, 0, 0, time.UTC),
			b:        time.Date(2017, 8, 2, 0, 23, 0, 0, time.UTC),
			expected: false,
		},
	}
	for _, tc := range cases {
		actual := IsSameDate(tc.a, tc.b)
		if actual != tc.expected {
			t.Fatalf("test case %s: timeA: %s , timeB: %s , actual: %t , expected: %t",
				tc.name,
				tc.a.Format(time.RFC3339),
				tc.b.Format(time.RFC3339),
				actual,
				tc.expected,
			)
		}
	}
}
