package timeutil

import (
	"strconv"
	"time"
)

// ParseUnixSeconds parses unix seconds from a string to a time.
func ParseUnixSeconds(seconds string) (time.Time, error) {
	t, err := strconv.ParseInt(seconds, 10, 64)
	if err != nil {
		return time.Time{}, err
	}
	return time.Unix(t, 0).UTC(), nil
}

// IsSameDate determines if two date times have the same day.
func IsSameDate(a, b time.Time) bool {
	ay, am, ad := a.Date()
	by, bm, bd := b.Date()
	return ay == by && am == bm && ad == bd
}
