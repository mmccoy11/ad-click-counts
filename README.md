# ad-click-counts

### What is this ?

This is a script to count how many clicks ads get on a single day.

### How to use

* Make sure you have [Go](https://golang.org/doc/install) installed.
    * Make sure you have your `$GOPATH` set.
    * Make sure you have `$GOPATH/bin` in your `$PATH`.
* Run `go get bitbucket.org/mmccoy11/ad-click-counts`
* The script has 3 flags
    * -date       : The date to search ad clicks for. Must be of the YYYY-MM-DD format.
    * -inputFile  : The input file of ad click data.
    * -outputFile : The output file of ad click counts. Defaults to ad-click-counts.csv.
* Example run of the script `ad-click-count -date 2017-08-04 -inputFile input.txt`

### Development

This project includes a Makefile to simplify running commands.

* Run `make build` to build a binary. It will be put into a bin directory of the project.
* Run `make test` to run tests.
* Run `make test-bench` to run tests and benchmarks.
* Run `make clean` to remove the bin directory.