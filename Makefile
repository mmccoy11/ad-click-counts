.PHONY: build clean test test-bench

all: clean test build

build:
	go build -o bin/ad-click-counts *.go

clean:
	rm -rf bin/

test:
	go test -v ./...

test-bench:
	go test -v ./... -bench .