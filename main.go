package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"time"

	"bitbucket.org/mmccoy11/ad-click-counts/counts"
)

const (
	dateFlagFormat        = "2006-01-02"
	defaultOutputFilename = "ad-click-counts.csv"
)

var (
	dateFlag       = flag.String("date", "", "The date to count clicks for")
	inputFileFlag  = flag.String("inputFile", "", "The file to read click data from")
	outputFileFlag = flag.String("outputfile", defaultOutputFilename, "The file to write click counts to")
)

func main() {
	flag.Parse()

	clickDate, err := time.Parse(dateFlagFormat, *dateFlag)
	if err != nil {
		fmt.Printf("Error parsing -date flag: %v\n", err)
		fmt.Println("Remember the -date flag is required and has YYYY-MM-DD format.")
		os.Exit(2)
	}

	if err := runCalculate(clickDate, *inputFileFlag, *outputFileFlag); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func runCalculate(clickDate time.Time, inputFilename, outputFilename string) error {
	inputFile, err := os.Open(inputFilename)
	if err != nil {
		return err
	}
	defer inputFile.Close()

	outputFile, err := os.Create(outputFilename)
	if err != nil {
		return err
	}
	defer outputFile.Close()

	csvReader := csv.NewReader(inputFile)
	csvWriter := csv.NewWriter(outputFile)
	return counts.Calculate(clickDate, csvReader, csvWriter)
}
