package counts

import (
	"bytes"
	"encoding/csv"
	"io/ioutil"
	"os"
	"testing"
	"time"
)

const (
	givenInputFilename  = "testdata/input.txt"
	givenOutputFilename = "testdata/output.txt"
)

var givenClickDate = time.Date(2017, 8, 4, 0, 0, 0, 0, time.UTC)

func TestGivenExample(t *testing.T) {
	inputFile, err := os.Open(givenInputFilename)
	if err != nil {
		t.Errorf("couldn't open input file %s because of %v", givenInputFilename, err)
	}
	defer inputFile.Close()

	expectedBytes, err := ioutil.ReadFile(givenOutputFilename)
	if err != nil {
		t.Errorf("couldn't read output file %s because of %v", givenOutputFilename, err)
	}

	byteBuffer := bytes.NewBuffer([]byte{})

	csvReader := csv.NewReader(inputFile)
	csvWriter := csv.NewWriter(byteBuffer)
	err = Calculate(givenClickDate, csvReader, csvWriter)
	if err != nil {
		t.Errorf("error calculating counts %v", err)
	}

	actualBytes, err := ioutil.ReadAll(byteBuffer)
	if err != nil {
		t.Errorf("err reading from byte buffer %v", err)
	}

	expectedString := string(expectedBytes)
	actualString := string(actualBytes)

	if expectedString != actualString {
		t.Errorf("not equal expected:\n%s \n\nactual:\n%s", expectedString, actualString)
	}
}

func BenchmarkGivenExample(b *testing.B) {
	for i := 0; i < b.N; i++ {
		b.StopTimer()

		inputFile, err := os.Open(givenInputFilename)
		if err != nil {
			b.Errorf("couldn't open input file %s because of %v", givenInputFilename, err)
		}

		byteBuffer := bytes.NewBuffer([]byte{})
		csvReader := csv.NewReader(inputFile)
		csvWriter := csv.NewWriter(byteBuffer)

		b.StartTimer()

		Calculate(givenClickDate, csvReader, csvWriter)
		inputFile.Close()
	}
}
