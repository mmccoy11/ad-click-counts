package counts

import (
	"encoding/csv"
	"io"
	"sort"
	"strconv"
	"time"

	"bitbucket.org/mmccoy11/ad-click-counts/timeutil"
)

type result struct {
	adID  int64
	count int64
}

// Calculate the ad click counts and write them to w.
func Calculate(clickDate time.Time, r *csv.Reader, w *csv.Writer) error {
	results, err := processRecords(clickDate, r)
	if err != nil {
		return err
	}

	var rs []result
	for _, r := range results {
		rs = append(rs, r)
	}
	sort.Slice(rs, func(i, j int) bool {
		return rs[i].adID < rs[j].adID
	})

	return writeResultsAsCSV(rs, w)
}

func processRecords(clickDate time.Time, r *csv.Reader) (map[int64]result, error) {
	results := make(map[int64]result)
	r.FieldsPerRecord = 3

	for {
		record, err := r.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return results, err
		}
		recordTime, err := timeutil.ParseUnixSeconds(record[0])
		if err != nil {
			return results, err
		}
		if timeutil.IsSameDate(recordTime, clickDate) {
			adID, err := strconv.ParseInt(record[2], 10, 64)
			if err != nil {
				return results, err
			}
			cc, ok := results[adID]
			if !ok {
				results[adID] = result{
					adID:  adID,
					count: 1,
				}
			} else {
				cc.count++
				results[adID] = cc
			}
		}
	}
	return results, nil
}

func writeResultsAsCSV(results []result, w *csv.Writer) error {
	for _, r := range results {
		countStr := strconv.FormatInt(r.count, 10)
		adIDStr := strconv.FormatInt(r.adID, 10)
		record := []string{adIDStr, countStr}
		if err := w.Write(record); err != nil {
			return err
		}
	}
	w.Flush()
	return w.Error()
}
